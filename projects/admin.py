from django.contrib import admin
from projects.models import Project


# Register your models here.
# ProjectsAdmin Register
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass
