from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from projects.views import list_projects
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
# Create new task View
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect(list_projects)
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


# List view for Task (login require)
@login_required
def show_my_tasks(request):
    mytasks = Task.objects.filter(assignee=request.user)
    context = {
        "mytasks": mytasks,
    }
    return render(request, "tasks/list.html", context)
